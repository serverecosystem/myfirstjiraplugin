package com.atlassian.tutorial.myFirstJiraPlugin.api;

public interface MyPluginComponent
{
    String getName();
}